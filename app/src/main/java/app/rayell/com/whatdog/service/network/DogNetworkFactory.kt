package app.rayell.com.whatdog.service.network

import android.arch.lifecycle.MutableLiveData
import app.rayell.com.whatdog.pojo.Breed
import app.rayell.com.whatdog.pojo.ImageDog

interface DogNetworkFactory {

    fun fetchAllBreed() : MutableLiveData<List<Breed>>
    fun fetchPictureByIdBreed(idBreed : Int) : List<ImageDog>
}