package app.rayell.com.whatdog.ui.fragment

import app.rayell.com.whatdog.pojo.Breed


interface DetailBreedFragmentI {
    fun showInformationOfBreed(breed: Breed)
}