package app.rayell.com.whatdog.service.network

import android.arch.lifecycle.MutableLiveData
import android.support.annotation.WorkerThread
import app.rayell.com.whatdog.Configurations
import app.rayell.com.whatdog.Configurations.API_KEY
import app.rayell.com.whatdog.Configurations.CONTENT_TYPE_KEY
import app.rayell.com.whatdog.Configurations.CONTENT_TYPE_VALUE
import app.rayell.com.whatdog.Configurations.HEADER_API_KEY
import app.rayell.com.whatdog.pojo.Breed
import app.rayell.com.whatdog.pojo.ImageDog
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.coroutines.CoroutineContext

class DogNetwork : DogNetworkFactory, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = job

    private var gson = GsonBuilder()
        .setLenient()
        .create()

    private val retrofit =
        Retrofit.Builder()
            .baseUrl(Configurations.HOSTNAME)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    private val dogApiService = retrofit.create(DogApiService::class.java)

    @WorkerThread
    override fun fetchAllBreed(): MutableLiveData<List<Breed>> {
        val liveData = MutableLiveData<List<Breed>>()

        launch {
            val allbreedResponse = dogApiService.fetchAllBreeds(getHeaderRequest())
            withContext(Dispatchers.Main) {
                if (allbreedResponse.isSuccessful) {
                    liveData.value = allbreedResponse.body()
                } else {
                    //throw DogException(allbreedResponse.code())
                }
            }
        }
        return liveData
    }

    override fun fetchPictureByIdBreed(idBreed: Int): List<ImageDog> {
        var listPicture = listOf<ImageDog>()
        launch {
            val pictureResponse = dogApiService.fetchPictureByIdBreed(idBreed.toString(),getHeaderRequest())
            withContext(Dispatchers.Main) {
                if (pictureResponse.isSuccessful) {
                    pictureResponse.body()?.let{
                        listPicture = it
                    }
                } else {
                    //throw DogException(allbreedResponse.code())
                }
            }
        }
        return listPicture
    }

    private fun getHeaderRequest(): Map<String, String> =
        HashMap<String, String>().apply {
            this[HEADER_API_KEY] = API_KEY
            this[CONTENT_TYPE_KEY] = CONTENT_TYPE_VALUE
        }
}