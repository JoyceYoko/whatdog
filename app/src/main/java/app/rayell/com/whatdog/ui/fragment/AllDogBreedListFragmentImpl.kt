package app.rayell.com.whatdog.ui.fragment

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import app.rayell.com.whatdog.R
import app.rayell.com.whatdog.extensions.RecyclerItemClickListener
import app.rayell.com.whatdog.extensions.onItemClick
import app.rayell.com.whatdog.extensions.replaceFragmentInActivity
import app.rayell.com.whatdog.pojo.Breed
import app.rayell.com.whatdog.presenters.BreedListPresenter
import app.rayell.com.whatdog.ui.DogActivity
import app.rayell.com.whatdog.ui.DogAdapter
import app.rayell.com.whatdog.ui.fragment.DetailBreedFragmentImp.Companion.DETAIL_BREED_TAG
import app.rayell.com.whatdog.viewModels.BreedViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class AllDogBreedListFragmentImpl : Fragment(), AllDogBreedListFragment, Observer<List<Breed>> {

    companion object {
        @JvmStatic
        fun newInstance(): AllDogBreedListFragmentImpl = AllDogBreedListFragmentImpl()
    }

    private val breedViewModel: BreedViewModel by viewModel()
    private lateinit var allDogsRecycleView: RecyclerView
    private lateinit var shimmerDogBreed: ShimmerFrameLayout
    private lateinit var layoutEmptyBreed: ConstraintLayout
    private lateinit var dogAdapter: DogAdapter
    private val presenter: BreedListPresenter by inject { parametersOf(this) }

    //region life cycle metyhods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_all_dog, container, false)
        initComponents(view)
        return view
    }

    override fun onStart() {
        super.onStart()
        super.onResume()
        activity?.let {
            (it as DogActivity).setTitleToolBar(getString(R.string.app_name))
            it.buttonBack.visibility = View.GONE
        }
        presenter.onResume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        breedViewModel.getAllBreeds().removeObserver(this)
    }

    //endregion
    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun startLoaderAnimation() {
        shimmerDogBreed.startShimmerAnimation()
        shimmerDogBreed.visibility = View.VISIBLE
    }

    override fun finishLoaderAnimation() {
        shimmerDogBreed.stopShimmerAnimation()
        shimmerDogBreed.visibility = View.GONE
    }

    //region private
    private fun initComponents(view: View) {
        allDogsRecycleView = view.findViewById(R.id.recyclerview_all_dogs)
        shimmerDogBreed = view.findViewById(R.id.shimmer_dog_breed)
        layoutEmptyBreed = view.findViewById(R.id.layout_empty_breed)
    }

    private fun initRecyclerView() {
        allDogsRecycleView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        allDogsRecycleView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    private fun initAdapter(allBreeds: List<Breed>) {
        if (::dogAdapter.isInitialized) {
            dogAdapter.notifyDataSetChanged(allBreeds)
        } else {
            dogAdapter = DogAdapter(allBreeds)
            allDogsRecycleView.adapter = dogAdapter
        }
    }

    override fun initObserverBreeds() {
        startLoaderAnimation()
        breedViewModel.getAllBreeds().observe(viewLifecycleOwner,this)
    }

    override fun onChanged(breeds: List<Breed>?) {
        breeds?.let {
            finishLoaderAnimation()
            if (it.isNotEmpty()) {
                showBreedInRecyclerView(it)
            } else {
                showViewEmptyBreed()
            }
        }
    }

    private fun showViewEmptyBreed() {
        allDogsRecycleView.visibility = View.GONE
        layoutEmptyBreed.visibility = View.VISIBLE
    }

    private fun showBreedInRecyclerView(breeds: List<Breed>) {
        initRecyclerView()
        initAdapter(breeds)
        initBreedsClickItemListener()
        allDogsRecycleView.visibility = View.VISIBLE
        layoutEmptyBreed.visibility = View.GONE
    }

    private fun initBreedsClickItemListener() {
        if (this::allDogsRecycleView.isInitialized) {
            allDogsRecycleView.apply {
                onItemClick(object : RecyclerItemClickListener.OnClickListener {
                    override fun onItemClick(position: Int, view: View) {
                        val item = dogAdapter.getItemAtPosition(position)
                        goToBreedDetails(item)
                    }
                })
            }
        }
    }

    override fun goToBreedDetails(item: Breed) {
        activity?.let { act ->
            (act as DogActivity).setTitleToolBar(getString(R.string.text_detail_breed_title).toUpperCase())
            (act).buttonBack.visibility = View.VISIBLE
            replaceFragmentInActivity(
                act.supportFragmentManager,
                DetailBreedFragmentImp.newInstance(item),
                R.id.frame_container_dog,
                true,
                DETAIL_BREED_TAG
            )
        }
    }
    //endregion
}