package app.rayell.com.whatdog

import android.app.Application
import app.rayell.com.whatdog.injection.*
import org.koin.android.ext.android.startKoin


class DogApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(
            this,
            listOf(
                viewModelModule,
                initPresenter,
                initDogReposirory,
                initNetwork,
                initDatabase,
                initPresenterDetail
            )
        )
    }
}