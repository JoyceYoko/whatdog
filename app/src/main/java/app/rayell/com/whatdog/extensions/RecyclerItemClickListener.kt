package app.rayell.com.whatdog.extensions

import android.support.v7.widget.RecyclerView
import android.view.View
import app.rayell.com.whatdog.Configurations.POSITION_ZERO

class RecyclerItemClickListener(private val mRecycler: RecyclerView, private val clickListener: OnClickListener? = null, private val longClickListener: OnLongClickListener? = null) :
    RecyclerView.OnChildAttachStateChangeListener {

    override fun onChildViewDetachedFromWindow(view: View) {
        view.setOnClickListener(null)
        view.setOnLongClickListener(null)
    }

    override fun onChildViewAttachedToWindow(view: View) {
        view.setOnClickListener { v ->
            setOnChildAttachedToWindow(v)
        }
    }

    private fun setOnChildAttachedToWindow(view: View?) {
        view?.let {
            val position = mRecycler.getChildLayoutPosition(it)
            if (position >= POSITION_ZERO) {
                clickListener?.onItemClick(position, it)
                longClickListener?.onLongItemClick(position, it)
            }
        }
    }

    interface OnClickListener {
        fun onItemClick(position: Int, view: View)
    }

    interface OnLongClickListener {
        fun onLongItemClick(position: Int, view: View)
    }
}
