package app.rayell.com.whatdog.service.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import app.rayell.com.whatdog.pojo.Breed


@Database(entities = [Breed::class], version = 1, exportSchema = false)
abstract class DatabaseBreed : RoomDatabase() {
    abstract fun breedDatabase(): Dao

}