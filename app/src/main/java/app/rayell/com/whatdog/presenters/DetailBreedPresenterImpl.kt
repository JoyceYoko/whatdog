package app.rayell.com.whatdog.presenters

import android.os.Bundle
import app.rayell.com.whatdog.Configurations
import app.rayell.com.whatdog.pojo.Breed
import app.rayell.com.whatdog.repository.DogRepository
import app.rayell.com.whatdog.ui.fragment.DetailBreedFragmentImp


class DetailBreedPresenterImpl(
    private val detailBreedFragment: DetailBreedFragmentImp,
    private val repository: DogRepository
) :
    DetailBreedPresenter {

    private var myBreedSelected: Breed? = null

    override fun extractDataFromBundle(arguments: Bundle?) {
        arguments?.let {
            myBreedSelected = it.getSerializable(Configurations.BREED_KEY_BUNDLE) as Breed
            detailBreedFragment.showInformationOfBreed(it.getSerializable(Configurations.BREED_KEY_BUNDLE) as Breed)
        }
    }

    override fun fetchPictureByIdBreed(): String {
        myBreedSelected?.let {
            val response = repository.fetchPictureByIdBreed(it.id)
            for (breedWithUrl in response) {
                return breedWithUrl.url
            }
        }
        return ""
    }

}