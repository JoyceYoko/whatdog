package app.rayell.com.whatdog.extensions

import android.support.v7.widget.RecyclerView

fun RecyclerView.onItemClick(listener: RecyclerItemClickListener.OnClickListener) {
    this.addOnChildAttachStateChangeListener(
        RecyclerItemClickListener(
            this,
            listener
        )
    )
}

fun RecyclerView.onLongItemClick(listener: RecyclerItemClickListener.OnLongClickListener) {
    this.addOnChildAttachStateChangeListener(
        RecyclerItemClickListener(
            this,
            longClickListener = listener
        )
    )
}


