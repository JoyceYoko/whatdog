package app.rayell.com.whatdog.ui.fragment

import app.rayell.com.whatdog.pojo.Breed


interface AllDogBreedListFragment  {

    fun finishLoaderAnimation()
    fun startLoaderAnimation()
    fun showToast(message: String)
    fun initObserverBreeds()
    fun goToBreedDetails(item: Breed)
}