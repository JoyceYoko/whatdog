package app.rayell.com.whatdog.repository

import android.arch.lifecycle.MutableLiveData
import app.rayell.com.whatdog.pojo.Breed
import app.rayell.com.whatdog.pojo.ImageDog
import app.rayell.com.whatdog.service.network.DogNetworkFactory


class DogRepositoryImpl(private val network: DogNetworkFactory) :
    DogRepository { //, private val database: DogDatabaseFactory) :


    override fun fetchAllBreed(): MutableLiveData<List<Breed>> = network.fetchAllBreed()

    override fun fetchPictureByIdBreed(idBreed: Int): List<ImageDog> =
        network.fetchPictureByIdBreed(idBreed)
}