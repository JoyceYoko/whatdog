package app.rayell.com.whatdog.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.rayell.com.whatdog.R
import app.rayell.com.whatdog.pojo.Breed

class DogAdapter(private var allBreeds: List<Breed>) : RecyclerView.Adapter<DogBreedViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): DogBreedViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.all_dog_cell, parent, false)
        return DogBreedViewHolder(itemView)
    }

    override fun getItemCount(): Int = allBreeds.count()

    fun getItemAtPosition(position: Int) = allBreeds[position]

    override fun onBindViewHolder(holder: DogBreedViewHolder, position: Int) {
        holder.setUpUi(allBreeds[position])
    }

    fun notifyDataSetChanged(breeds: List<Breed>) {
        allBreeds = breeds
        super.notifyDataSetChanged()
    }
}

class DogBreedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val nameBreed: TextView by lazy {
        itemView.findViewById(R.id.name_breed) as TextView
    }

    fun setUpUi(breed: Breed) {
        nameBreed.text = breed.name
    }
}
