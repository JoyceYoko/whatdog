package app.rayell.com.whatdog.ui.fragment

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.rayell.com.whatdog.Configurations.BREED_KEY_BUNDLE
import app.rayell.com.whatdog.R
import app.rayell.com.whatdog.pojo.Breed
import app.rayell.com.whatdog.presenters.DetailBreedPresenter
import com.facebook.shimmer.ShimmerFrameLayout
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class DetailBreedFragmentImp : Fragment(), DetailBreedFragmentI {


    companion object {
        const val DETAIL_BREED_TAG = "DetailBreedFragment"
        @JvmStatic
        fun newInstance(breedSelected: Breed): DetailBreedFragmentImp {
            return DetailBreedFragmentImp().apply {
                arguments = Bundle().apply {
                    putSerializable(BREED_KEY_BUNDLE, breedSelected)
                }
            }
        }
    }

    //region fileds
    private lateinit var shimmerDogDetailBreed: ShimmerFrameLayout
    private lateinit var layoutEmptyBreed: ConstraintLayout
    private lateinit var imgDetailBreed: ImageView
    private lateinit var txtTemperamentValue: TextView
    private lateinit var txtDescriptionValue: TextView
    private lateinit var txtLifeSpanValue: TextView
    private lateinit var txtGroupValue: TextView
    private lateinit var txtNameBreed: TextView
    private val detailBreedPresenter: DetailBreedPresenter by inject { parametersOf(this) }
    //endregion

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail_breed, container, false)
        initComponents(view)
        return view
    }

    private fun initComponents(view: View) {
        shimmerDogDetailBreed = view.findViewById(R.id.shimmer_dog_detail_breed)
        layoutEmptyBreed = view.findViewById(R.id.layout_empty_breed)
        imgDetailBreed = view.findViewById(R.id.img_detail_breed)
        txtTemperamentValue = view.findViewById(R.id.txt_temperament_value)
        txtDescriptionValue = view.findViewById(R.id.txt_description_value)
        txtLifeSpanValue = view.findViewById(R.id.txt_life_span_value)
        txtGroupValue = view.findViewById(R.id.txt_group)
        txtNameBreed = view.findViewById(R.id.txt_name_breed)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailBreedPresenter.extractDataFromBundle(arguments)
        //detailBreedPresenter.fetchPictureByIdBreed() probleme json
    }

    override fun showInformationOfBreed(breed: Breed) {
        txtTemperamentValue.text = breed.temperament
        txtDescriptionValue.text = breed.description
        txtLifeSpanValue.text = breed.lifeSpan
        txtGroupValue.text = breed.breedGroup
        txtNameBreed.text = breed.name
    }
}