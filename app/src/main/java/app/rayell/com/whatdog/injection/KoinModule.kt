package app.rayell.com.whatdog.injection

import android.arch.persistence.room.Room
import app.rayell.com.whatdog.presenters.BreedListPresenter
import app.rayell.com.whatdog.presenters.BreedListPresenterImpl
import app.rayell.com.whatdog.presenters.DetailBreedPresenter
import app.rayell.com.whatdog.presenters.DetailBreedPresenterImpl
import app.rayell.com.whatdog.repository.DogRepository
import app.rayell.com.whatdog.repository.DogRepositoryImpl
import app.rayell.com.whatdog.service.database.DatabaseBreed
import app.rayell.com.whatdog.service.network.DogNetwork
import app.rayell.com.whatdog.service.network.DogNetworkFactory
import app.rayell.com.whatdog.ui.fragment.AllDogBreedListFragment
import app.rayell.com.whatdog.ui.fragment.DetailBreedFragmentImp
import app.rayell.com.whatdog.viewModels.BreedViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

val viewModelModule: Module = module {
    viewModel { BreedViewModel(androidApplication(), get<DogRepository>()) }
}

val initPresenter: Module = module {
    single { (view: AllDogBreedListFragment) -> BreedListPresenterImpl(androidContext(), view) as BreedListPresenter
    }
}

val initPresenterDetail: Module = module {
    single { (view: DetailBreedFragmentImp) -> DetailBreedPresenterImpl(view, get()) as DetailBreedPresenter
    }
}

val initDatabase = module {
    single {
        Room.databaseBuilder(androidApplication(), DatabaseBreed::class.java, "breed_database.dbb").build()
    }
    single { get<DatabaseBreed>().breedDatabase() }
}

val initDogReposirory = module {
    single { DogRepositoryImpl(get<DogNetworkFactory>()) as DogRepository }
}

val initNetwork = module {
    single { DogNetwork() as DogNetworkFactory }
}