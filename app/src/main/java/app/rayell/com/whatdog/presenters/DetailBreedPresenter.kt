package app.rayell.com.whatdog.presenters

import android.os.Bundle


interface DetailBreedPresenter {
    fun extractDataFromBundle(arguments: Bundle?)
    fun fetchPictureByIdBreed(): String
}