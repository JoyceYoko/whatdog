package app.rayell.com.whatdog.service.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import app.rayell.com.whatdog.pojo.Breed

@Dao
interface DogDatabaseFactory {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBreeds(listBreeds: List<Breed>)

    @Query("SELECT * FROM table_breed")
    fun getBreeds(): LiveData<List<Breed>>
}