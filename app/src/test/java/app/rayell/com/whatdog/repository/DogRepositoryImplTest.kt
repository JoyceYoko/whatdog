package app.rayell.com.whatdog.repository

import android.arch.lifecycle.MutableLiveData
import app.rayell.com.whatdog.pojo.Breed
import app.rayell.com.whatdog.pojo.ImageDog
import app.rayell.com.whatdog.service.network.DogNetworkFactory
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Test
import org.mockito.Mockito.verify

class DogRepositoryImplTest {

    @Test
    fun `fetch all Breed from ws and they return list of Breed`() {
        val response = MutableLiveData<List<Breed>>()
        val dogNetwork = mock<DogNetworkFactory>()

        whenever(dogNetwork.fetchAllBreed()).thenReturn(response)

        val brochureFetcher = DogRepositoryImpl(dogNetwork)
        brochureFetcher.fetchAllBreed()

        verify(dogNetwork).fetchAllBreed()
    }

    @Test
    fun `fetch image by breed id from ws and they return list of imageBreed`() {
        val breedId = 1
        val response :List<ImageDog> = arrayListOf()
        val dogNetwork = mock<DogNetworkFactory>()

        whenever(dogNetwork.fetchPictureByIdBreed(breedId)).thenReturn(response)

        val brochureFetcher = DogRepositoryImpl(dogNetwork)
        brochureFetcher.fetchPictureByIdBreed(breedId)

        verify(dogNetwork).fetchPictureByIdBreed(breedId)
    }
}