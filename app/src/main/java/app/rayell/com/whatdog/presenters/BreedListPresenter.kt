package app.rayell.com.whatdog.presenters

interface BreedListPresenter {
    fun fetchAllBreed()
    fun onResume()
}
