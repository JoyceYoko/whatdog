package app.rayell.com.whatdog.viewModels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import app.rayell.com.whatdog.repository.DogRepository

class BreedViewModel(application: Application, private val repository: DogRepository) :
    AndroidViewModel(application) {

    fun getAllBreeds() = repository.fetchAllBreed()
}