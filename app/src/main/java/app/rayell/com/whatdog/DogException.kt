package app.rayell.com.whatdog


private const val CODE_ERROR_400 = 400
class DogException(): Exception() {

    enum class DogExceptionExceptionCode {
        NetworkError, BadRequestError
    }
    var exceptionCode: Any? = null

    constructor(exceptionCode: Int): this()  {
        setExceptionCodeFromNetworkStatus(exceptionCode)
    }

    private fun setExceptionCodeFromNetworkStatus(networkCode: Int) {
        when (networkCode) {
            CODE_ERROR_400 -> this.exceptionCode = DogExceptionExceptionCode.BadRequestError
            else -> this.exceptionCode = DogExceptionExceptionCode.NetworkError
        }
    }
}