package app.rayell.com.whatdog.service.network

import app.rayell.com.whatdog.pojo.Breed
import app.rayell.com.whatdog.pojo.ImageDog
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Query


interface DogApiService {

    @GET("v1/breeds")
    suspend fun fetchAllBreeds(@HeaderMap headerMap: Map<String, String>): Response<List<Breed>>

    @GET("v1/images/search?format=src&mime_types=small")
    suspend fun fetchPictureByIdBreed(@Query("breed_id") breed_id: String, @HeaderMap headerMap: Map<String, String>): Response<List<ImageDog>>
}