package app.rayell.com.whatdog.presenters

import android.content.Context
import app.rayell.com.whatdog.ui.fragment.AllDogBreedListFragment
import com.nhaarman.mockito_kotlin.mock
import org.junit.Test
import org.mockito.Mockito

class BreedListPresenterImplTest {
    @Test
    fun `test onresume user has connection`() {
        val context = mock<Context>()
        val allDogBreedListFragment = mock<AllDogBreedListFragment>()

        val presenter = BreedListPresenterImpl(context, allDogBreedListFragment)
        presenter.userIsconnected = true
        presenter.onResume()

        Mockito.verify(allDogBreedListFragment).initObserverBreeds()
    }
}