package app.rayell.com.whatdog.presenters

import android.content.Context
import app.rayell.com.whatdog.Connectivity
import app.rayell.com.whatdog.R
import app.rayell.com.whatdog.ui.fragment.AllDogBreedListFragment


open class BreedListPresenterImpl(private var context: Context, private var allDogListFragmentFactory: AllDogBreedListFragment) :
    BreedListPresenter {

    var userIsconnected = Connectivity.isDeviceConnected(context)

    override fun onResume() {
        if(userIsconnected){
            fetchAllBreed()
        } else {
            allDogListFragmentFactory.showToast(context.getString(R.string.text_not_connection))
        }
    }

    override fun fetchAllBreed() {
        allDogListFragmentFactory.initObserverBreeds()
    }
}