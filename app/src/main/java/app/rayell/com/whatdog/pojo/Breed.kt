package app.rayell.com.whatdog.pojo

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "table_breed")
data class Breed(
    @SerializedName("bredFor")
    val bredFor: String,
    @SerializedName("breedGroup")
    val breedGroup: String,
    @SerializedName("height")
    val height: Height,
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("lifeSpan")
    val lifeSpan: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("origin")
    val origin: String,
    @SerializedName("temperament")
    val temperament: String,
    @SerializedName("weight")
    val weight: Weight,
    @SerializedName("countryCode")
    val countryCode: String,
    @SerializedName("description")
    val description: String
) : Serializable

data class Height(
    @SerializedName("imperial")
    var imperial: String,
    @SerializedName("metric")
    var metric: String
) : Serializable

data class Weight(
    @SerializedName("imperial")
    var imperial: String,
    @SerializedName("metric")
    var metric: String
) : Serializable

data class ImageDog(
    @SerializedName("id")
    var id: String,
    @SerializedName("url")
    var url: String,
    @SerializedName("breeds")
    var breeds: List<Breed> = listOf()
) : Serializable