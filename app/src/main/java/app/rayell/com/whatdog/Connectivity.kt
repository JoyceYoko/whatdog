package app.rayell.com.whatdog

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo


object Connectivity {
    private fun isNetworkAvailable(networkInfo: NetworkInfo) = networkInfo.let { it.detailedState == NetworkInfo.DetailedState.BLOCKED && it.extraInfo == null && it.isAvailable }

    fun isDeviceConnected(context: Context): Boolean {
    val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = connMgr.activeNetworkInfo
    return networkInfo?.let { it.isConnected || isNetworkAvailable(it) } ?: false
}
}