package app.rayell.com.whatdog


object Configurations {
    const val API_KEY = "93bc7dbe-212e-41a7-a3ce-96e936d69b3d"
    const val CONTENT_TYPE_VALUE = "application/json"
    const val HOSTNAME = "https://api.thedogapi.com/"
    const val HEADER_API_KEY = "x-api-key"
    const val CONTENT_TYPE_KEY = "Content-Type"

    const val POSITION_ZERO = 0
    const val BREED_KEY_BUNDLE = "BREED"
}