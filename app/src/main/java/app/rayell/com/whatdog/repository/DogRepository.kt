package app.rayell.com.whatdog.repository

import android.arch.lifecycle.MutableLiveData
import app.rayell.com.whatdog.pojo.Breed
import app.rayell.com.whatdog.pojo.ImageDog


interface DogRepository {
    fun fetchAllBreed() : MutableLiveData<List<Breed>>
    fun fetchPictureByIdBreed(idBreed : Int): List<ImageDog>
}