package app.rayell.com.whatdog.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import app.rayell.com.whatdog.R
import app.rayell.com.whatdog.extensions.replaceFragmentInActivity
import app.rayell.com.whatdog.ui.fragment.AllDogBreedListFragmentImpl

class DogActivity : AppCompatActivity() {

    lateinit var buttonBack: ImageView
    lateinit var titletoolBar: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dog)

        initComponents()
        initUI()
        initListeners()
    }

    fun setTitleToolBar(value: String) {
        titletoolBar.text = value
    }

    private fun initComponents() {
        buttonBack = findViewById(R.id.ic_back)
        titletoolBar = findViewById(R.id.title_toolbar)
    }

    //region Private methods
    private fun initUI() {
        replaceFragmentInActivity(
            supportFragmentManager,
            AllDogBreedListFragmentImpl.newInstance(),
            R.id.frame_container_dog,
            true
        )
    }

    private fun initListeners() {
        buttonBack.setOnClickListener { onBackPressed() }
    }
}