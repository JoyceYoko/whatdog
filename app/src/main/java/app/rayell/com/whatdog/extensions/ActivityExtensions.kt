package app.rayell.com.whatdog.extensions

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager


@Synchronized
fun replaceFragmentInActivity(fragmentManager: FragmentManager, fragment: Fragment, frameId: Int, shouldAddToBackStack: Boolean = false, fragmentTag: String? = null, enterAnim: Int = 0, exitAnim: Int = 0,
                              allowStateLoss: Boolean = true, slideIn: Int = 0, slideOut: Int = 0) {
    val isFinishing = fragment.activity?.isFinishing ?: false
    if (!fragment.isVisible && !isFinishing) {
        val transaction = fragmentManager.beginTransaction()
        transaction.setCustomAnimations(enterAnim, exitAnim, slideIn, slideOut)
        transaction.replace(frameId, fragment, fragmentTag)
        if (shouldAddToBackStack && fragmentTag != null) {
            transaction.addToBackStack(fragmentTag)
        }
        if (allowStateLoss) {
            transaction.commitAllowingStateLoss()
        } else {
            transaction.commit()
        }
    }
}